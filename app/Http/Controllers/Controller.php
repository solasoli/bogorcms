<?php

namespace App\Http\Controllers;

use App\Models\Users;
use App\Models\UsersAccess;
use App\Models\UsersLevelGroup;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function IsDeleted($table, $id)
    {
        return DB::table($table)->whereId($id)->update(['is_deleted' => 1, 'deleted_by' => Auth::user()->id, 'deleted_date' => date('Y-m-d H:i:s')]);
    }

    public function select_data($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    public function DateTime()
    {
        return date('Y-m-d H:i:s');
    }

    public function Auth()
    {
        $q = Users::select("*")->with("users_level")->where("id", Auth::user()->id)->first();
        return $q;
    }

    public function AuthUsersLevel()
    {
        $q = UsersLevelGroup::select("*")->with("users_level")->where("users_id", Auth::user()->id);
        return $q;
    }

    public function CheckAllowAccess($kode_menu='', $access='')
    {
        $users_level_id = Auth::user()->users_level_id;
        $q = DB::table('users_access as ua')
        ->select(["ua.id"])
        ->join("users_menu as um", "um.id", "=", "ua.users_menu_id")
        ->where("ua.users_level_id", $users_level_id)
        ->where("um.kode_menu", $kode_menu)
        ->where("ua.access", $access)
        ->first();  
        return $q;
    }
}
