<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Models\RefKlasifikasiKategori as myData;
use Illuminate\Support\Facades\Auth;

class RefKlasifikasiKategoriController extends Controller
{
	var $table = "ref_klasifikasi_kategori";
	var $kodeMenu = "M002003";

	function index()
	{
        if ($this->CheckAllowAccess($this->kodeMenu, 'show') == null){
            abort(404);
        }
		$data = array(
			'firstMenu' => 'M002',
			'secondMenu' => $this->kodeMenu,
			'access_add' => $this->CheckAllowAccess($this->kodeMenu, "add"),
			'access_edit' => $this->CheckAllowAccess($this->kodeMenu, "edit"),
			'access_delete' => $this->CheckAllowAccess($this->kodeMenu, "delete"),
		);
		return view('pages.ref-klasifikasi-kategori', $data);
	}

	public function where(Request $request)
	{
		$result['error'] = 3;
		$result['message'] = "Error Method";
		$result['status_code'] = 203;
		$status_validation = false;
		$validation_rules = [
            'id' => 'min:1|max:20|numeric',
			'limit' => 'max:1|numeric',
			'count' => 'max:1|numeric',
		];
		$validator = Validator::make($request->all(), $validation_rules);
		if ($validator->fails()) {
			$result['error_validation'] = $validator->errors();
			$status_validation = true;
		}
		if ($status_validation == true) {
			$result['error'] = 1;
			$result['message'] = "Data is not found";
			return response()->json($result);exit;
		}
		$q = myData::select(["*"])->with(["created_by_user" => function($query){
			$query->select(["id", "name"]);
		}])->with(["last_modified_by_user" => function($query){
			$query->select(["id", "name"]);
		}]);
		if ($request->id) {
			$q->where("id", $request->id);
		}
		$q = $q->get();
		if (count($q) > 0) {
			$result['error'] = 0;
			$result['message'] = "Successfully Read Data";
			$result['status_code'] = 202;
		}else{
			$result['error'] = 2;
			$result['message'] = "No data";
			$result['status_code'] = 202;
		}
		$result['total_data'] = count($q);
		$result['data'] = $q;
		return response()->json($result);
	}

	public function list(Request $request)
	{
        $access_detail = $this->CheckAllowAccess($this->kodeMenu, "detail");
        $access_edit = $this->CheckAllowAccess($this->kodeMenu, "edit");
        $access_delete = $this->CheckAllowAccess($this->kodeMenu, "delete");
		$data = myData::select(["id", 'nama_klasifikasi_kategori', 'created_by', 'last_modified_by'])->with('created_by_user')->with('last_modified_by_user');
		$data = $data->where("is_deleted", 0);
        if ($request->nama_klasifikasi_kategori) {
            $data->where('nama_klasifikasi_kategori', 'LIKE', "%$request->nama_klasifikasi_kategori%");
        }
		return DataTables::eloquent($data)
		->addColumn('opsi', function ($d) use($access_detail, $access_edit, $access_delete) {
			$opsi = '';
			if ($access_detail) {
				$opsi .= "<button class='btn btn-sm btn-success' onclick='Detail(\"" . $d->id . "\")'>Detail</button> ";
			}
			if ($access_edit) {
				$opsi .= "<button class='btn btn-sm btn-info' onclick='Edit(\"" . $d->id . "\")'>Edit</button> ";
			}
			if ($access_delete) {
				$opsi .= "<button class='btn btn-sm btn-danger' onclick='Delete(\"" . $d->id . "\")'>Delete</button>";
			}
			return $opsi;
		})->escapeColumns([])
		->toJson();
	}

	public function add(Request $request)
	{
		$result['error'] = 3;
		$result['message'] = "Error Requests";
		$result['status_code'] = 203;
        if ($this->CheckAllowAccess($this->kodeMenu, 'add') == null){
			$result['error'] = 6;
			$result['message'] = "Not allowed to add";
			$result['status_code'] = 201;
			return response()->json($result);exit;
        }
		$status_validation = false;
		$data_validation = array_map('e',
			array(
				'nama_klasifikasi_kategori' => $request->nama_klasifikasi_kategori,
			)
		);
		$validation_rules = [
            'nama_klasifikasi_kategori' => ['required', 'max:200',
				Rule::unique($this->table)->where(function ($query) use ($request) {
					return $query
						->whereNamaKlasifikasiKategori($request->nama_klasifikasi_kategori)
						->whereIsDeleted(0);
				}),
			],
		];
		$validator = Validator::make($data_validation, $validation_rules);
		$validator->setAttributeNames([
			'nama_klasifikasi_kategori' => 'Klasifikasi Kategori',
		]);
		if ($validator->fails()) {
			$result['error_validation'] = $validator->errors();
			$status_validation = true;
		}
		if ($status_validation == true) {
			$result['error'] = 1;
			$result['message'] = "Data is not found";
		}else{
			$data_validation['created_by'] = Auth::user()->id;
			$data_validation['created_date'] = $this->DateTime();
			if (myData::create($data_validation)) {
				$result['error'] = 0;
				$result['message'] = "Successfully Create Data";
			}else{
				$result['error'] = 1;
				$result['message'] = "Error Create Data";
			}
		}
		return response()->json($result);
	}

	public function update(Request $request)
	{
		$result['error'] = 3;
		$result['message'] = "Error Requests";
		$result['status_code'] = 203;
        if ($this->CheckAllowAccess($this->kodeMenu, 'edit') == null){
			$result['error'] = 6;
			$result['message'] = "Not allowed to update";
			$result['status_code'] = 201;
			return response()->json($result);exit;
        }
		$status_validation = false;
		$data_validation = array_map('e',array(
			'id' => $request->id,
			'nama_klasifikasi_kategori' => $request->nama_klasifikasi_kategori,
		));
		$validation_rules = [
			'id' => 'required|digits_between:1,20|numeric',
            'nama_klasifikasi_kategori' => ['required', 'max:200',
				Rule::unique($this->table)->where(function ($query) use ($request) {
					return $query
						->whereNamaKlasifikasiKategori($request->nama_klasifikasi_kategori)
						->whereIsDeleted(0)
						->where("id", '!=', $request->id);
				}),
			],
		];
		$validator = Validator::make($data_validation, $validation_rules);
		$validator->setAttributeNames([
			'id' => 'Id',
			'nama_klasifikasi_kategori' => 'Klasifikasi Kategori',
		]);
		if ($validator->fails()) {
			$result['error_validation'] = $validator->errors();
			$status_validation = true;
		}
		if ($status_validation == true) {
			$result['error'] = 1;
			$result['message'] = "Data is not valid";
		}else{
			$myData = myData::whereId(e($request->id))->first();
			if ($myData === null) {
				$result['error'] = 2;
				$result['message'] = "Data is not found";
			}else{
				$data_validation['last_modified_by'] = Auth::user()->id;
				$data_validation['last_modified_date'] = $this->DateTime();
				if (myData::whereId(e($request->id))->update($data_validation)) {
					$result['error'] = 0;
					$result['message'] = "Successfully Update Data";
				}else{
					$result['error'] = 1;
					$result['message'] = "Error Update Data";
				}
			}
		}
		return response()->json($result);
	}

	public function delete(Request $request, $id)
	{
		$result['error'] = 3;
		$result['message'] = "Error Requests";
		$result['status_code'] = 203;
        if ($this->CheckAllowAccess($this->kodeMenu, 'delete') == null){
			$result['error'] = 6;
			$result['message'] = "Not allowed to delete";
			$result['status_code'] = 201;
			return response()->json($result);exit;
        }
		$status_validation = false;
		$data_validation = array(
			'id' => e($id),
		);
		$validation_rules = [
			'id' => 'required|digits_between:1,2|numeric',
		];
		$validator = Validator::make($data_validation, $validation_rules);
		if ($validator->fails()) {
			$result['error_validation'] = $validator->errors();
			$status_validation = true;
		}
		if ($status_validation == true) {
			$result['error'] = 1;
			$result['message'] = "Data is not valid";
		}else{
			$myData = myData::whereId(e($id))->first();
			if ($myData === null) {
				$result['error'] = 2;
				$result['message'] = "Data is not found";
			}else{
				if ($this->IsDeleted($this->table, $id)) {
					$result['error'] = 0;
					$result['message'] = "Successfully Delete Data";
				}else{
					$result['error'] = 1;
					$result['message'] = "Error Delete Data";
				}
			}
		}
		return response()->json($result);
	}

	public function delete_selected(Request $request)
	{
		$result['error'] = 3;
		$result['message'] = "Error Requests";
		$result['status_code'] = 203;
        if ($this->CheckAllowAccess($this->kodeMenu, 'delete') == null){
			$result['error'] = 6;
			$result['message'] = "Not allowed to delete";
			$result['status_code'] = 201;
			return response()->json($result);exit;
        }
		$status_validation = false;
		foreach ($request->id as $key => $value) {
			$id = e($value);
			$data_validation = array(
				'id' => $id,
			);
			$validation_rules = [
				'id' => 'required|digits_between:1,2|numeric',
			];
			$validator = Validator::make($data_validation, $validation_rules);
			if ($validator->fails()) {
				$result['error_validation'] = $validator->errors();
				$status_validation = true;
			}
			if ($status_validation == true) {
				$result['error'] = 1;
				$result['message'] = "Data is not valid";
			}else{
				$myData = myData::whereId(e($id))->first();
				if ($myData === null) {
					$result['error'] = 2;
					$result['message'] = "Data is not found";
				}else{
					if ($this->IsDeleted($this->table, $id)) {
						$result['error'] = 0;
						$result['message'] = "Successfully Delete Data";
					}else{
						$result['error'] = 1;
						$result['message'] = "Error Delete Data";
						return response()->json($result);exit;
					}
				}
			}	
		}
		return response()->json($result);
	}
}
