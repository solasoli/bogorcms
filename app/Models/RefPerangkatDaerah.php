<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RefPerangkatDaerah extends Model
{
    use HasFactory;
	protected $table = "ref_perangkat_daerah";
	protected $primaryKey = "id";
	public $timestamps = false;
    protected $guarded = [];

	public function created_by_user()
	{
		return $this->belongsTo(User::class, 'created_by')->select(['id', 'name']);
	}

	public function last_modified_by_user()
	{
		return $this->belongsTo(User::class, 'last_modified_by')->select(['id', 'name']);
	}
}
