<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    use HasFactory;
	protected $table = "users";
	protected $primaryKey = "id";
	public $timestamps = false;
    protected $guarded = [];
    protected $hidden = [
        'password',
		'remember_token',
    ];

	public function created_by_user()
	{
		return $this->belongsTo(User::class, 'created_by')->select(['id', 'name']);
	}

	public function last_modified_by_user()
	{
		return $this->belongsTo(User::class, 'last_modified_by')->select(['id', 'name']);
	}

	public function is_konfirmasi_by_user()
	{
		return $this->belongsTo(User::class, 'is_konfirmasi_by')->select(['id', 'name']);
	}

	public function users_level()
	{
		return $this->belongsTo(UsersLevel::class, 'users_level_id')->select(['id', 'name_level']);
	}

	public function perangkat_daerah()
	{
		return $this->belongsTo(RefPerangkatDaerah::class, 'perangkat_daerah_id')->select(['id', 'nama_perangkat_daerah']);
	}

	public function element_masyarakat()
	{
		return $this->belongsTo(RefElementMasyarakat::class, 'element_masyarakat_id')->select(['id', 'nama_element_masyarakat']);
	}

	public function users_level_group()
	{
		return $this->hasMany(UsersLevelGroup::class, "users_id")->select(["id", "users_id", "users_level_id"]);
	}

}
