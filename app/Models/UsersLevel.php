<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UsersLevel extends Model
{
    use HasFactory;
	protected $table = "users_level";
	protected $primaryKey = "id";
	public $timestamps = false;
    protected $guarded = [];

	public function created_by_user()
	{
		return $this->belongsTo(Users::class, 'created_by')->select(['id', 'name']);
	}

	public function last_modified_by_user()
	{
		return $this->belongsTo(Users::class, 'last_modified_by')->select(['id', 'name']);
	}
}
