<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UsersLevelGroup extends Model
{
    use HasFactory;
	protected $table = "users_level_group";
	protected $primaryKey = "id";
	public $timestamps = false;
    protected $guarded = [];

	public function created_by_user()
	{
		return $this->belongsTo(Users::class, 'created_by')->select(['id', 'name']);
	}

	public function last_modified_by_user()
	{
		return $this->belongsTo(Users::class, 'last_modified_by')->select(['id', 'name']);
	}

    public function users_level()
    {
        return $this->belongsTo(UsersLevel::class)->select(["id", 'name_level']);
    }

    public function users()
    {
        return $this->belongsTo(Users::class, 'users_id')->select(["id", 'name', 'email']);
    }
}
