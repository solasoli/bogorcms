<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\RefKlasifikasiKategori;

class RefKlasifikasiKategoriFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */

    protected $model = RefKlasifikasiKategori::class;

    public function definition()
    {
        return [
            'nama_klasifikasi_kategori' => $this->faker->jobTitle,
            'created_by' => 1,
            'created_date' => now(),
        ];
    }
}
