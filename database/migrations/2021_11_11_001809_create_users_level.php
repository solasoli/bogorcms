<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersLevel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_level', function (Blueprint $table) {
            $table->id();
            $table->string('name_level', 100)->index();
            $table->integer('sequence', false, false)->length(11);
            $table->boolean("allow_login")->default(true);
            $table->integer("created_by", false, false)->length(11)->nullable()->index();
            $table->dateTime("created_date")->nullable();
            $table->integer("last_modified_by", false, false)->length(11)->nullable()->index();
            $table->dateTime("last_modified_date")->nullable();
            $table->boolean("is_deleted")->default(false)->index();
            $table->integer("deleted_by", false, false)->length(11)->nullable()->index();
            $table->dateTime("deleted_date")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_level');
    }
}
