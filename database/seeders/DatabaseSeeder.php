<?php
namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([UsersSeeder::class]);
        $this->call([UsersLevelSeeder::class]);
        $this->call([UsersMenuSeeder::class]);
        $this->call([RefElementMasyarakatSeeder::class]);
        $this->call([RefPerangkatDaerahSeeder::class]);
        $this->call([RefKlasifikasiKategoriSeeder::class]);
    }
}
