<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\RefElementMasyarakat;

class RefElementMasyarakatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RefElementMasyarakat::factory(10)->create();
    }
}
