<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\RefPerangkatDaerah;

class RefPerangkatDaerahSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RefPerangkatDaerah::factory(10)->create();
    }
}
