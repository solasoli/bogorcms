<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users_menu')->insert([
            'kode_menu' => 'M001',
            'name_menu' => 'Dashboard',
            'url' => 'dashboard',
            'rel' => 0,
            'sequence' => 1,
            'created_date' => now(),
        ]);
        DB::table('users_menu')->insert([
            'kode_menu' => 'M002',
            'name_menu' => 'Master',
            'url' => '#', 
            'rel' => 0,
            'sequence' => 1,
            'created_date' => now(),
        ]);
        DB::table('users_menu')->insert([
            'kode_menu' => 'M002001',
            'name_menu' => 'Enum',
            'url' => 'enum', 
            'rel' => 2,
            'sequence' => 1,
            'created_date' => now(),
        ]);
    }
}
