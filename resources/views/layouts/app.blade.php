<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Bank Inovasi Sukabumi | {{$titlePage}}</title>
    <link href='{{ asset('/css/main.css') }}' rel='stylesheet' />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('material') }}/img/apple-icon.png">
    {{-- <link rel="icon" type="image/png" href="{{ asset('material') }}/img/favicon.png"> --}}
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <!-- CSS Files -->
    <link href="{{ asset('material') }}/css/material-dashboard.css?v=2.1.1" rel="stylesheet" />
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="{{ asset('material') }}/demo/demo.css" rel="stylesheet" />

    <style>
        #DataTable thead tr th {
            /* text-align: center; */
            vertical-align: middle;
            white-space: nowrap;
        }

        #DataTable tbody tr td {
            white-space: nowrap;
            vertical-align: middle;
        }

        #DataTable tfoot tr th {
            vertical-align: middle;
            white-space: nowrap;
        }

        #DataTable tfoot tr td {
            vertical-align: middle;
            white-space: nowrap;
        }

        .dataTables_filter {
            text-align: right;
            font-size: 11pt;
        }

        .dataTables_filter .form-control {
            width: 100%;
        }

        div.dataTables_wrapper div.dataTables_length select {
            width: auto;
            display: inline-block;
        }

        div.dataTables_wrapper div.dataTables_filter input {
            margin-left: 0.5em;
            display: inline-block;
            width: auto;
        }

        div.dataTables_wrapper div.dataTables_paginate ul.pagination {
            margin: 2px 0;
            white-space: nowrap;
            justify-content: flex-end;
        }

        .card {
            color: inherit;
        }

        .btn-step {
            width: 100%;
        }

        .table thead tr th {
            font-size: 14px;
        }

        .table-detail tr th {
            vertical-align: middle;
            white-space: nowrap;
        }

        .table-detail tr th:nth-child(2) {
            text-align: center;
        }
    </style>
</head>

<body class="{{ $class ?? '' }}">
    @auth()
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
    </form>
    @include('layouts.page_templates.auth')
    @endauth
    @guest()
    @include('layouts.page_templates.guest')
    @endguest
    @if (auth()->check())
    @endif

    <!--   Core JS Files   -->
    <script src="{{ asset('material') }}/js/core/jquery.min.js"></script>
    <script src="{{ asset('material') }}/js/core/popper.min.js"></script>
    <script src="{{ asset('material') }}/js/core/bootstrap-material-design.min.js"></script>
    <script src="{{ asset('material') }}/js/plugins/perfect-scrollbar.jquery.min.js"></script>
    <!-- Plugin for the momentJs  -->
    <script src="{{ asset('material') }}/js/plugins/moment.min.js"></script>
    <!--  Plugin for Sweet Alert -->
    <script src="{{ asset('material') }}/js/plugins/sweetalert2.js"></script>
    <!-- Forms Validations Plugin -->
    <script src="{{ asset('material') }}/js/plugins/jquery.validate.min.js"></script>
    <!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
    <script src="{{ asset('material') }}/js/plugins/jquery.bootstrap-wizard.js"></script>
    <!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
    <script src="{{ asset('material') }}/js/plugins/bootstrap-selectpicker.js"></script>
    <!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
    <script src="{{ asset('material') }}/js/plugins/bootstrap-datetimepicker.min.js"></script>
    <!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->

    <script src="{{ asset('material') }}/js/plugins/jquery.dataTables.min.js" defer></script>
    <!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
    <script src="{{ asset('material') }}/js/plugins/bootstrap-tagsinput.js"></script>
    <!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
    <script src="{{ asset('material') }}/js/plugins/jasny-bootstrap.min.js"></script>
    <!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
    <script src="{{ asset('material') }}/js/plugins/fullcalendar.min.js"></script>
    <!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
    <script src="{{ asset('material') }}/js/plugins/jquery-jvectormap.js"></script>
    <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
    <script src="{{ asset('material') }}/js/plugins/nouislider.min.js"></script>
    <!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
    <!-- Library for adding dinamically elements -->
    <script src="{{ asset('material') }}/js/plugins/arrive.min.js"></script>
    <!--  Google Maps Plugin    -->
    {{-- <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE'"></script> --}}
    <!-- Chartist JS -->
    <script src="{{ asset('material') }}/js/plugins/chartist.min.js"></script>
    <!--  Notifications Plugin    -->
    <script src="{{ asset('material') }}/js/plugins/bootstrap-notify.js"></script>
    <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
    <script src="{{ asset('material') }}/js/material-dashboard.js?v=2.1.1" type="text/javascript"></script>
    <!-- Material Dashboard DEMO methods, don't include it in your project! -->
    <script src="{{ asset('material') }}/demo/demo.js"></script>
    <script src="{{ asset('material') }}/js/settings.js"></script>
    {{-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
    </script> --}}
    <script src="{{ asset('js/main.js') }}"></script>

    <!--Toastr-->
    <link href="{{asset('css/toastr/toastr.css')}}" rel="stylesheet">
    <script src="{{asset('js/toastr/toastr.js')}}"></script>
    <!---->

    <link rel="stylesheet" href="{{ asset('summernote') }}/summernote-bs4.css">
    <script src="{{ asset('summernote') }}/summernote-bs4.min.js"></script>
    @stack('js')

    <script type="text/javascript">
        $(document).ready(function() {

            $('#form input').keyup(function() {
                $(this).popover('hide');
            });

            $('#form select').on('change', function() {
                $(this).popover('hide');
            });

            $('#form input').on('change', function() {
                $(this).popover('hide');
            });

            $.ajaxSetup({
                dataType: 'json',
                beforeSend: function(xhr) {
                    // xhr.setRequestHeader("auth_key","<?php echo csrf_token() ?>"); 
                },
                data: {
                    "_token": "<?php echo csrf_token() ?>",
                }
            });
        });

        function ValidationPopover(theobject, message) {
            $(theobject).attr('data-content', message);
            $(theobject).popover({
                trigger: 'manual',
                placement: 'top'
            });
            $(theobject).popover('show');
        }

        function Logout() {
            Swal.fire({
                title: 'Are you sure?',
                text: "Keluar Aplikasi!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes!'
            }).then((result) => {
                console.log(result)
                if (result.value) {
                    $("#logout-form").submit();
                }
            });
        }

        function SwitchLevel(id) {
            Swal.fire({
                title: 'Are you sure?',
                text: "Ganti akses level!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes!'
            }).then((result) => {
                console.log(result)
                if (result.value) {
                    $.ajax({
                        url: "{{ url('/switch_level')}}/" + id,
                        type: 'PUT',
                        success: function(data) {
                            if (data.error == 0) {
                                window.location.href = "{{ url('/home')}}";
                            } else {
                                toastr.warning(data.message);
                            }
                        },
                        error: function(data) {
                            toastr.error("Error Switch Level");
                        }
                    });
                }
            });
        }
    </script>
    @yield('script')

</body>

</html>