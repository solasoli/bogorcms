<div class="sidebar" data-color="orange" data-background-color="white" data-image="{{ asset('/images/skbm.jpg') }}">
    <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
    <div class="logo">
        {{-- <a href="https://creative-tim.com/" class="simple-text logo-normal">
      {{ __('Creative Tim') }}
        </a> --}}
        {{-- <img class="text-center" src="{{ asset('/images/kabupaten-sukabumi.png') }}" width="100 px"> --}}
        <h5 class="text-center">Bank Inovasi Pemerintah Kabupaten Sukabumi</h5>
    </div>


    <div class="sidebar-wrapper">
        <ul class="nav">
            <?php
            $GetMenuAccess = \App\Http\Controllers\UsersManagement\UsersMenuController::GetMenuAccess();
            ?>
            @foreach ($GetMenuAccess as $key => $value)
            @if ($value->url != "#")
            <li class="nav-item {{ @$firstMenu == $value->kode_menu ? ' active' : '' }}">
                <a class="nav-link" href="{{ route($value->url) }}">
                    @if ($value->icon)
                    <i class="material-icons">{{$value->icon}}</i>
                    @endif
                    <p>{{ $value->name_menu }}</p>
                </a>
            </li>
            @else
            <li class="nav-item {{ @$firstMenu == $value->kode_menu ? ' active' : '' }}">
                <a class="nav-link" data-toggle="collapse" href="#{{$value->kode_menu}}" aria-expanded="{{ @$firstMenu == $value->kode_menu ? 'true' : 'false' }}">
                    {{-- <i><img style="width:25px" src="{{ asset('material') }}/img/laravel.svg"></i> --}}
                    @if ($value->icon)
                    <i class="material-icons">{{$value->icon}}</i>
                    @endif
                    <p>{{ $value->name_menu }}
                        <b class="caret"></b>
                    </p>
                    
                </a>
                <div class="collapse {{ @$firstMenu == $value->kode_menu ? ' show' : '' }}" id="{{$value->kode_menu}}">
                    <ul class="nav">
                        <?php
                        $GetMenuAccess_sub = \App\Http\Controllers\UsersManagement\UsersMenuController::GetMenuAccess($value->id);
                        ?>
                        @foreach ($GetMenuAccess_sub as $k => $v)
                        <li class="nav-item {{ $secondMenu == $v->kode_menu ? ' active' : '' }}">
                            <a class="nav-link" href="{{ url($v->url) }}">
                                {{-- <span class="sidebar-mini"> UP </span> --}}

                                @if ($v->icon)
                                <i class="material-icons">{{$v->icon}}</i>
                                @endif
                                <span class="sidebar-normal">{{$v->name_menu}} </span>
                            </a>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </li>
            @endif
            @endforeach
            {{-- <li class="nav-item{{ $activePage == 'table' ? ' active' : '' }}">
                <a class="nav-link" href="{{ route('table') }}">
                    <i class="material-icons">content_paste</i>
                    <p>{{ __('Proposal Inovasi') }}</p>
                </a>
            </li>
            <li class="nav-item{{ $activePage == 'judul-proposal' ? ' active' : '' }}">
                <a class="nav-link" href="/proposal">
                    <i class="material-icons">library_books</i>
                    <p>{{ __('Submit Proposal') }}</p>
                </a>
            </li>
            <li class="nav-item{{ $activePage == 'icons' ? ' active' : '' }}">
                <a class="nav-link" href="{{ route('icons') }}">
                    <i class="material-icons">bubble_chart</i>
                    <p>{{ __('Kalender Event') }}</p>
                </a>
            </li> --}}
        </ul>
    </div>
</div>