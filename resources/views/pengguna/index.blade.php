@extends('layouts.app', [
'firstMenu' => $firstMenu,
'secondMenu' => $secondMenu,
'titlePage' => __('Pengguna')])

@section('content')
<style>
    #DataTable tbody tr td:nth-child(1),
    #DataTable tbody tr td:nth-child(2) {
        text-align: center;
    }
</style>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title">Pengguna</h4>
                        {{-- <p class="card-category"> Here is a subtitle for this table</p> --}}
                    </div>
                    <div class="card-body">

                        @if ($access_add)
                        <button type="button" class="btn btn-sm btn-primary" onclick="Add()" data-trigger="hover" data-toggle="popover" data-placement="right" data-content="Add Data" data-original-title="">Add</button>
                        @endif

                        <button type="button" class="btn btn-info btn-sm" onclick="Refresh()" data-trigger="hover" data-toggle="popover" data-placement="right" data-content="Refresh Data"> Refresh</button>

                        @if ($access_delete)
                        <button type="button" id="DeleteSelected" class="btn btn-sm btn-danger" onclick="DeleteSelected()" data-trigger="hover" data-toggle="popover" data-placement="right" data-content="Delete Selected" style="display: none;">Delete Selected</button>
                        @endif

                        <table class="table table-striped table-hover DataTable" id="DataTable" style="width: 100%">
                            <thead>
                                <tr>
                                    <th width="5%" style="vertical-align: middle;">No.</th>
                                    <th width="5%" class="text-center">
                                        <input type="checkbox" id="CheckAll" style="position: relative;left: 0px;opacity: 1;">
                                    </th>
                                    <th width="5%" class="text-center">Opsi</th>
                                    <th>Nama Penanggung Jawab</th>
                                    <th>Email</th>
                                    <th>Perangkat Daerah</th>
                                    <th>Jenis Pengguna</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-form">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Form</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <form id="form" class="form-horizontal">
                @csrf
                <input type="hidden" name="id">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="bmd-form-group">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="material-icons">face</i>
                                        </span>
                                    </div>
                                    <input type="text" name="name" class="form-control" placeholder="Nama Penanggung Jawab" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="bmd-form-group mt-3">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="material-icons">email</i>
                                        </span>
                                    </div>
                                    <input type="email" name="email" class="form-control" placeholder="Email" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="bmd-form-group mt-3">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="material-icons">fax</i>
                                        </span>
                                    </div>
                                    <input type="text" name="no_telp" class="form-control" placeholder="No. Telp/Handphone" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="bmd-form-group mt-3">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="material-icons">lock_outline</i>
                                        </span>
                                    </div>
                                    <input type="password" name="password" id="password" class="form-control" placeholder="Kata Sandi">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="bmd-form-group mt-3">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="material-icons">lock_outline</i>
                                        </span>
                                    </div>
                                    <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" placeholder="Konfirmasi Kata Sandi">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="bmd-form-group mt-3">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="material-icons">apartment</i>
                                </span>
                            </div>
                            <select class="form-control" name="perangkat_daerah_id">
                                <option value="">Pilih Jenis Pengguna</option>
                                @foreach ($listPerangkatDaerah as $item)
                                    <option value="{{$item->id}}">{{$item->nama_perangkat_daerah}}</option>
                                @endforeach
                            </select>
                            <input type="text" name="instansi" id="instansi" class="form-control" placeholder="Instansi" style="display:none">
                            <div class="checkbox" style="padding-top:10px;">
                                <label>
                                    <input type="checkbox" value="1" id="lainnya" name="lainnya">
                                    Lainnya
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="bmd-form-group mt-3">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="material-icons">group_add</i>
                                </span>
                            </div>
                            <select class="form-control" name="element_masyarakat_id">
                                <option value="">Pilih Jenis Pengguna</option>
                                @foreach ($listElementMasyarakat as $item)
                                <option value="{{$item->id}}">{{$item->nama_element_masyarakat}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="bmd-form-group mt-3">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="material-icons">flag</i>
                                        </span>
                                    </div>
                                    <select class="form-control" name="status" required>
                                        <option value="">Pilih Status Aktif</option>
                                        <option value="Aktif">Aktif</option>
                                        <option value="Nonaktif">Nonaktif</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-detail">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Detail Data</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <div class="form-group box-foto">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-4 offset-md-4">
                                <center>
                                    <img id="file_foto" class="img-responsive thumbnail img-responsive" width="100%">
                                </center>
                            </div>
                        </div>
                    </div>
                </div>
                <table class="table table-detail table-striped">
                    <tr>
                        <th width="5%">Nama Penanggung Jawab</th>
                        <th width="1%">:</th>
                        <td class="name"></td>
                    </tr>
                    <tr>
                        <th>Email</th>
                        <th>:</th>
                        <td class="email"></td>
                    </tr>
                    <tr>
                        <th>No. Telp</th>
                        <th>:</th>
                        <td class="no_telp"></td>
                    </tr>
                    <tr>
                        <th>Status</th>
                        <th>:</th>
                        <td class="status"></td>
                    </tr>
                    <tr>
                        <th>Instansi</th>
                        <th>:</th>
                        <td class="instansi"></td>
                    </tr>
                    <tr>
                        <th>Jenis Pengguna</th>
                        <th>:</th>
                        <td class="nama_element_masyarakat"></td>
                    </tr>
                    <tr>
                        <th>Konfirmasi By</th>
                        <th>:</th>
                        <td class="is_konfirmasi_by_name"></td>
                    </tr>
                    <tr>
                        <th>Konfirmasi Date</th>
                        <th>:</th>
                        <td class="is_konfirmasi_date"></td>
                    </tr>
                    <tr>
                        <th>Created By</th>
                        <th>:</th>
                        <td class="created_by_name"></td>
                    </tr>
                    <tr>
                        <th>Created Date</th>
                        <th>:</th>
                        <td class="created_date"></td>
                    </tr>
                    <tr>
                        <th>Last Modified By</th>
                        <th>:</th>
                        <td class="last_modified_by_name"></td>
                    </tr>
                    <tr>
                        <th>Last Modified Date</th>
                        <th>:</th>
                        <td class="last_modified_date"></td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@endsection
@section('script')
<script>
    var table = "";
    var module_api = "<?php echo url('/pengguna'); ?>";
    var save_type = "Add";
    $(document).ready(function() {
        $('#form').on("submit", function(event) {
            event.preventDefault();
            var url;
            if (save_type == "Add") {
                var url = ""
            } else {
                var url = "/update"
            }
            ResetValidation();
            Swal.fire({
                title: 'Are you sure?',
                text: "Save Data",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes!'
            }).then((result) => {
                console.log(result)
                if (result.value) {
                    setTimeout(() => {
                        $.ajax({
                            url: module_api + url,
                            data: new FormData($('#form')[0]),
                            type: 'post',
                            contentType: false,
                            processData: false,
                            async: false,
                            dataType: 'json',
                            success: function(data) {
                                if (data.error == 0) {
                                    ResetForm();
                                    table.ajax.reload(null, false);
                                    $("#modal-form").modal('hide');
                                    toastr.info(data.message);
                                } else if (data.error == 1) {
                                    setTimeout(() => {
                                        $.each(data.error_validation, function(key, value) {
                                            ValidationPopover('[name=' + key + ']', value);
                                        });
                                    }, 0);
                                    toastr.warning(data.message);
                                } else {
                                    toastr.warning(data.message);
                                }
                                swal.close();
                            },
                            error: function(data) {
                                swal.close();
                                toastr.error("Error " + save_type + " Data");
                            }
                        });
                    }, 150);
                }
            })
        });

        $("#lainnya").change(function() {
            if (this.checked == true) {
                $("[name=instansi]").fadeIn(0);
                $("[name=instansi]").attr("required", true);
                $("[name=perangkat_daerah_id]").fadeOut(0);
                $("[name=perangkat_daerah_Id]").attr("required", false);
            }else{
                $("[name=instansi]").fadeOut(0);
                $("[name=instansi]").attr("required", false);
                $("[name=perangkat_daerah_id]").fadeIn(0);
                $("[name=perangkat_daerah_Id]").attr("required", true);
            }
        });
        table = $('#DataTable').DataTable({
            'lengthMenu': [
                [10, 25, 50, 100, 200, 350, -1],
                [10, 25, 50, 100, 200, 350, "All"]
            ],
            "language": {
                "emptyTable": "<center>Data not found</center>",
                "processing": '<center><i class="fa fa-refresh fa-spin fa-3x fa-fw" style="font-size:16pt;"></i></center>',

            },
            'searching': true,
            'processing': true,
            'serverSide': true,
            "orderCellsTop": true,
            "paging": true,
            'order': [],
            'ajax': {
                url: module_api + '/list',
                type: "POST",
                data: function(d) {},
                "error": function(jqXHR) {}
            },
            "fnDrawCallback": function(oSettings) {
                $('#DataTable').find('input:checkbox').prop('checked', false);
                CheckTotalCheckedDelete();
            },
            "columns": [{
                    "data": null,
                    render: function(data, type, row, meta) {
                        return +meta.row + meta.settings._iDisplayStart + 1;
                    },
                },
                {
                    "data": "id",
                    render: function(data, type, row, meta) {
                        return "<input type='checkbox' id='CheckboxRow' onclick='CheckTotalCheckedDelete()' data-id='" + row.id + "' style='position: relative;left: 0px;opacity: 1;'/>";
                    },
                },
                {
                    "data": "opsi"
                },
                {
                    "data": "name"
                },
                {
                    "data": "email"
                },
                {
                    "data": "instansi",
                },
                {
                    "data": "element_masyarakat.nama_element_masyarakat"
                },
            ],
            "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "targets": [0, 1, 2],
                }
                @if(!$access_delete), {
                    "targets": [1],
                    "visible": false
                }
                @endif
            ],
        });
        $('#DataTable').wrap("<div class='table-responsive'></div>");
        $('#DeleteSelected').fadeOut(0);
        $("#CheckAll").click(function() {
            $('#DataTable').find('input:checkbox').not(this).prop('checked', this.checked);
            CheckTotalCheckedDelete();
        });
    });

    function Refresh() {
        table.ajax.reload(null, false);
    }

    function CheckTotalCheckedDelete() {
        var TotalOfCheckBoxRow = $('input#CheckboxRow').length;
        var TotalOfChecked = $('input#CheckboxRow:checked').length;
        if (TotalOfChecked > 0) {
            $('#DeleteSelected').fadeIn(0);
            if (TotalOfCheckBoxRow == TotalOfChecked) {
                $('#CheckAll').prop('checked', true);
            } else {
                $('#CheckAll').prop('checked', false);
            }
        } else {
            $('#DeleteSelected').fadeOut(0);
            $('#CheckAll').prop('checked', false);
        }
    }

    function Reset() {
        if (save_type == "Update") {
            ResetForm();
        }
        setTimeout(() => {}, 0);
    }

    function ResetValidation() {
        $('#form').find('input').popover('dispose');
        $('#form').find('select').popover('dispose');
    }

    function ResetForm() {
        $('#form')[0].reset();
        ResetValidation();
        setTimeout(() => {}, 0);
    }

    function Add() {
        Reset();
        save_type = "Add";
        $("#modal-form").modal('show');
        $("[name=password]").attr('required', true);
        $("[name=confirm_password]").attr('required', true);
    }

    function Edit(value) {
        ResetForm();
        $("[name=password]").attr('required', false);
        $("[name=confirm_password]").attr('required', false);
        $.ajax({
            url: "<?php echo url('/users/where'); ?>",
            type: 'GET',
            data: {
                id: value,
            },
            success: function(data) {
                if (data.error == 0) {
                    var d = data.data[0];
                    $('[name=id]').val(d.id);
                    $('[name=name]').val(d.name);
                    $('[name=email]').val(d.email);
                    $('[name=no_telp]').val(d.no_telp);
                    if (d.instansi) {
                        $("[name=lainnya]").prop("checked", true);
                        $("[name=instansi]").val(d.instansi);
                        $("[name=instansi]").fadeIn();
                        $("[name=perangkat_daerah_id]").fadeOut();
                    }else{
                        $("[name=lainnya]").prop("checked", false);
                        $("[name=perangkat_daerah_id]").val(d.perangkat_daerah_id);
                        $("[name=perangkat_daerah_id]").fadeIn();                        
                        $("[name=instansi]").fadeOut();
                    }
                    $('[name=element_masyarakat_id]').val(d.element_masyarakat_id);
                    $('[name=status]').val(d.status);
                    $("#modal-form").modal('show');
                    save_type = "Update";
                } else {
                    toastr.warning(data.message);
                }
            },
            error: function(data) {
                toastr.error("Error Edit Data");
            }
        });
    }

    function Detail(value) {
        $.ajax({
            url: "<?php echo url('/users/where'); ?>",
            type: 'GET',
            data: {
                id: value,
                using_users_level_group: 1,
            },
            success: function(data) {
                if (data.error == 0) {
                    $("#modal-detail").modal('show');
                    var d = data.data[0];
                    $('.name').text(d.name);
                    $('.email').text(d.email);
                    $('.no_telp').text(d.no_telp);
                    $('.status').text(d.status);
                    $('.instansi').text((d.instansi) ? d.instansi : d.perangkat_daerah.nama_perangkat_daerah);
                    $('.nama_element_masyarakat').text((d.element_masyarakat) ? d.element_masyarakat.nama_element_masyarakat : null);
                    $('.is_konfirmasi_by_name').text((d.is_konfirmasi_by_user) ? d.is_konfirmasi_by_user.name : null);
                    $('.is_konfirmasi_date').text(d.is_konfirmasi_date);
                    $('.created_by_name').text((d.created_by_user) ? d.created_by_user.name : null);
                    $('.created_date').text(d.created_date);
                    $('.last_modified_by_name').text(d.last_modified_by_user ? d.last_modified_by_user.name : null);
                    $('.last_modified_date').text(d.last_modified_date);
                    if (d.file_foto) {
                        $(".box-foto").fadeIn(0);
                        $("#file_foto").attr('src', "<?php echo url('/uploads/users/foto'); ?>" + "/" + d.file_foto);
                    } else {
                        $(".box-foto").fadeOut(0);
                    }
                } else {
                    toastr.warning(data.message);
                }
            },
            error: function(data) {
                toastr.error("Error Detail Data");
            }
        });
    }

    function Delete(value) {
        Swal.fire({
            title: 'Are you sure?',
            text: "Delete Data",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!'
        }).then((result) => {
            console.log(result)
            if (result.value) {
                $.ajax({
                    url: module_api + "/" + value,
                    method: 'delete',
                    success: function(data) {
                        if (data.error == 0) {
                            table.ajax.reload(null, false);
                            toastr.info(data.message);
                        } else {
                            toastr.warning(data.message);
                        }
                        swal.close();
                    },
                    error: function(data) {
                        toastr.error('Error Delete Data');
                        swal.close();
                    }
                });
            }
        });
    }

    function DeleteSelected() {
        var ValChecked = [];
        $('#CheckboxRow:checked').each(function() {
            ValChecked.push($(this).attr('data-id'));
        });
        Swal.fire({
            title: 'Are you sure?',
            text: "Delete Data",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!'
        }).then((result) => {
            console.log(result)
            if (result.value) {
                $.ajax({
                    url: module_api + "/delete_selected",
                    method: 'post',
                    data: {
                        id: ValChecked
                    },
                    success: function(data) {
                        if (data.error == 0) {
                            table.ajax.reload(null, false);
                            toastr.info(data.message);
                        } else {
                            toastr.warning(data.message);
                        }
                        swal.close();
                    },
                    error: function(data) {
                        toastr.error("Error Delete Selected Data");
                        swal.close();
                    }
                });
            }
        })
    }

    function LoginAs(value) {
        Swal.fire({
            title: 'Are you sure?',
            text: "Login As!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!'
        }).then((result) => {
            console.log(result)
            if (result.value) {
                $.ajax({
                    url: module_api + "/login_as/" + value,
                    method: 'post',
                    success: function(data) {
                        if (data.error == 0) {
                            window.location.href = "{{url('/home')}}";
                        } else {
                            toastr.warning(data.message);
                        }
                        swal.close();
                    },
                    error: function(data) {
                        toastr.error('Error Login As Data');
                        swal.close();
                    }
                });
            }
        });
    }
</script>
@endsection