@extends('layouts.app', [
'firstMenu' => $firstMenu,
'secondMenu' => $secondMenu,
'titlePage' => __('Pengguna')])

@section('content')
<style>
    #DataTable tbody tr td:nth-child(1),
    #DataTable tbody tr td:nth-child(2) {
        text-align: center;
    }
</style>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title">Pengguna Menunggu Konfirmasi</h4>
                        {{-- <p class="card-category"> Here is a subtitle for this table</p> --}}
                    </div>
                    <div class="card-body">

                        <button type="button" class="btn btn-info btn-sm" onclick="Refresh()" data-trigger="hover" data-toggle="popover" data-placement="right" data-content="Refresh Data"> Refresh</button>

                        <div class='btn-group btn-sm' id="DeleteSelected" role='group' aria-label='Basic example' style='padding:0px;display:none;'>
                            <button type='button' class='btn btn-primary btn-sm' onclick='KonfirmasiAll(1)'>Konfirmasi</button>
                            <button type='button' class='btn btn-danger btn-sm' onclick='KonfirmasiAll(2)'>Tolak</button>
                        </div>";

                        <table class="table table-striped table-hover DataTable" id="DataTable" style="width: 100%">
                            <thead>
                                <tr>
                                    <th width="5%" style="vertical-align: middle;">No.</th>
                                    <th width="5%" class="text-center">
                                        <input type="checkbox" id="CheckAll" style="position: relative;left: 0px;opacity: 1;">
                                    </th>
                                    <th width="5%" class="text-center">Opsi</th>
                                    <th>Nama Penanggung Jawab</th>
                                    <th>Email</th>
                                    <th>Perangkat Daerah</th>
                                    <th>Jenis Pengguna</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-detail">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Detail Data</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <div class="form-group box-foto">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-4 offset-md-4">
                                <center>
                                    <img id="file_foto" class="img-responsive thumbnail img-responsive" width="100%">
                                </center>
                            </div>
                        </div>
                    </div>
                </div>
                <table class="table table-detail table-striped">
                    <tr>
                        <th width="5%">Nama Penanggung Jawab</th>
                        <th width="1%">:</th>
                        <td class="name"></td>
                    </tr>
                    <tr>
                        <th>Email</th>
                        <th>:</th>
                        <td class="email"></td>
                    </tr>
                    <tr>
                        <th>No. Telp</th>
                        <th>:</th>
                        <td class="no_telp"></td>
                    </tr>
                    <tr>
                        <th>Status</th>
                        <th>:</th>
                        <td class="status"></td>
                    </tr>
                    <tr>
                        <th>Instansi</th>
                        <th>:</th>
                        <td class="instansi"></td>
                    </tr>
                    <tr>
                        <th>Jenis Pengguna</th>
                        <th>:</th>
                        <td class="nama_element_masyarakat"></td>
                    </tr>
                    <tr>
                        <th>Created By</th>
                        <th>:</th>
                        <td class="created_by_name"></td>
                    </tr>
                    <tr>
                        <th>Created Date</th>
                        <th>:</th>
                        <td class="created_date"></td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@endsection
@section('script')
<script>
    var table = "";
    var module_api = "<?php echo url('/pengguna/konfirmasi'); ?>";
    var save_type = "Add";
    $(document).ready(function() {
        table = $('#DataTable').DataTable({
            'lengthMenu': [
                [10, 25, 50, 100, 200, 350, -1],
                [10, 25, 50, 100, 200, 350, "All"]
            ],
            "language": {
                "emptyTable": "<center>Data not found</center>",
                "processing": '<center><i class="fa fa-refresh fa-spin fa-3x fa-fw" style="font-size:16pt;"></i></center>',

            },
            'searching': true,
            'processing': true,
            'serverSide': true,
            "orderCellsTop": true,
            "paging": true,
            'order': [],
            'ajax': {
                url: module_api + '/list',
                type: "POST",
                data: function(d) {},
                "error": function(jqXHR) {}
            },
            "fnDrawCallback": function(oSettings) {
                $('#DataTable').find('input:checkbox').prop('checked', false);
                CheckTotalCheckedDelete();
            },
            "columns": [{
                    "data": null,
                    render: function(data, type, row, meta) {
                        return +meta.row + meta.settings._iDisplayStart + 1;
                    },
                },
                {
                    "data": "id",
                    render: function(data, type, row, meta) {
                        return "<input type='checkbox' id='CheckboxRow' onclick='CheckTotalCheckedDelete()' data-id='" + row.id + "' style='position: relative;left: 0px;opacity: 1;'/>";
                    },
                },
                {
                    "data": "opsi"
                },
                {
                    "data": "name"
                },
                {
                    "data": "email"
                },
                {
                    "data": "instansi"
                },
                {
                    "data": "element_masyarakat.nama_element_masyarakat"
                },
            ],
            "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "targets": [0, 1, 2],
                }
                @if(!$access_delete), {
                    "targets": [1],
                    "visible": false
                }
                @endif
            ],
        });
        $('#DataTable').wrap("<div class='table-responsive'></div>");
        $('#DeleteSelected').fadeOut(0);
        $("#CheckAll").click(function() {
            $('#DataTable').find('input:checkbox').not(this).prop('checked', this.checked);
            CheckTotalCheckedDelete();
        });
    });

    function Refresh() {
        table.ajax.reload(null, false);
    }

    function CheckTotalCheckedDelete() {
        var TotalOfCheckBoxRow = $('input#CheckboxRow').length;
        var TotalOfChecked = $('input#CheckboxRow:checked').length;
        if (TotalOfChecked > 0) {
            $('#DeleteSelected').fadeIn(0);
            if (TotalOfCheckBoxRow == TotalOfChecked) {
                $('#CheckAll').prop('checked', true);
            } else {
                $('#CheckAll').prop('checked', false);
            }
        } else {
            $('#DeleteSelected').fadeOut(0);
            $('#CheckAll').prop('checked', false);
        }
    }

    function Detail(value) {
        $.ajax({
            url: "<?php echo url('/users/where'); ?>",
            type: 'GET',
            data: {
                id: value,
                using_users_level_group: 1,
            },
            success: function(data) {
                if (data.error == 0) {
                    $("#modal-detail").modal('show');
                    var d = data.data[0];
                    $('.name').text(d.name);
                    $('.email').text(d.email);
                    $('.no_telp').text(d.no_telp);
                    $('.status').text(d.status);
                    $('.instansi').text(d.instansi);
                    $('.nama_element_masyarakat').text(d.element_masyarakat.nama_element_masyarakat);
                    $('.created_by_name').text((d.created_by_user) ? d.created_by_user.name : null);
                    $('.created_date').text(d.created_date);
                    $('.last_modified_by_name').text(d.last_modified_by_user ? d.last_modified_by_user.name : null);
                    $('.last_modified_date').text(d.last_modified_date);
                    if (d.file_foto) {
                        $(".box-foto").fadeIn(0);
                        $("#file_foto").attr('src', "<?php echo url('/uploads/users/foto'); ?>" + "/" + d.file_foto);
                    } else {
                        $(".box-foto").fadeOut(0);
                    }
                } else {
                    toastr.warning(data.message);
                }
            },
            error: function(data) {
                toastr.error("Error Detail Data");
            }
        });
    }

    function Konfirmasi(value, is_konfirmasi) {
        var ket = (is_konfirmasi == 1) ? 'Konfirmasi Pengguna?' : 'Tolak Pengguna';
        Swal.fire({
            title: 'Apakah anda yakin?',
            text: ket,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!'
        }).then((result) => {
            console.log(result)
            if (result.value) {
                $.ajax({
                    url: module_api + "/" + value + "/" + is_konfirmasi,
                    method: 'put',
                    success: function(data) {
                        if (data.error == 0) {
                            table.ajax.reload(null, false);
                            toastr.info(data.message);
                        } else {
                            toastr.warning(data.message);
                        }
                        swal.close();
                    },
                    error: function(data) {
                        toastr.error('Error Delete Data');
                        swal.close();
                    }
                });
            }
        });
    }

    function KonfirmasiAll(is_konfirmasi) {
        var ket = (is_konfirmasi == 1) ? 'Konfirmasi Pengguna?' : 'Tolak Pengguna';
        var ValChecked = [];
        $('#CheckboxRow:checked').each(function() {
            ValChecked.push($(this).attr('data-id'));
        });
        Swal.fire({
            title: 'Apakah anda yakin?',
            text: ket,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!'
        }).then((result) => {
            console.log(result)
            if (result.value) {
                $.ajax({
                    url: module_api + "/selected",
                    method: 'post',
                    data: {
                        id: ValChecked,
                        is_konfirmasi: is_konfirmasi,
                    },
                    success: function(data) {
                        if (data.error == 0) {
                            table.ajax.reload(null, false);
                            toastr.info(data.message);
                        } else {
                            toastr.warning(data.message);
                        }
                        swal.close();
                    },
                    error: function(data) {
                        toastr.error("Error Delete Selected Data");
                        swal.close();
                    }
                });
            }
        })
    }
</script>
@endsection