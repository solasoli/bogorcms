@extends('layouts.app', [
    'firstMenu' => $firstMenu,
    'secondMenu' => $secondMenu, 
    'titlePage' => __('Users Access')])

@section('content')
<style>
    input[type=checkbox] {
        transform: scale(2);
    }
    #DataTable tbody tr td:nth-child(1),
    #DataTable tbody tr td:nth-child(5),
    #DataTable tbody tr td:nth-child(6),
    #DataTable tbody tr td:nth-child(7),
    #DataTable tbody tr td:nth-child(8),
    #DataTable tbody tr td:nth-child(9),
    #DataTable tbody tr td:nth-child(10),
    #DataTable tbody tr td:nth-child(11) {
        text-align: center;
    }
</style>
<div class="content">
    <div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title ">Data Users Access</h4>
                    {{-- <p class="card-category"> Here is a subtitle for this table</p> --}}
                </div>
                <div class="card-body">

                    <button type="button" id="DeleteSelected" class="btn btn-sm btn-danger" onclick="DeleteSelected()" data-trigger="hover" data-toggle="popover" data-placement="right" data-content="Delete Selected" style="display: none;">Delete Selected</button>

                    <center>
                        <div class="form-group">
                            <div class="dropdown">
                                <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    {{(@$myUsersLevel->name_level) ? @$myUsersLevel->name_level : 'Pilih Users Level'}}
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    @foreach ($listUsersLevel as $key => $value)
                                    @if ($value->id != @$myUsersLevel->id)
                                    <a class="dropdown-item" href="{{url('/users-access?users_level_id='.$value->id)}}">{{$value->name_level}}</a>
                                    @endif
                                    @endforeach
                                </div>
                                @if (@$myUsersLevel->id)
                                <button class="btn btn-success" onclick="Save()">Save</button>
                                @endif
                            </div>
                        </div>
                    </center>
                    @if (@$myUsersLevel->id)
                        <table class="table table-striped table-hover DataTable" id="DataTable" style="width: 100%">
                            <thead>
                                <tr>
                                    <th width="5%" style="vertical-align: middle;" rowspan="2">No.</th>
                                    <th rowspan="2">Kode Menu</th>
                                    <th rowspan="2">Name Menu</th>
                                    <th rowspan="2">Url</th>
                                    <th rowspan="2" width="5%">Icon</th>
                                    <th rowspan="2" width="5%">Sequence</th>
                                    <th width="6%">Show</th>
                                    <th width="6%">Add</th>
                                    <th width="6%">Edit</th>
                                    <th width="6%">Detail</th>
                                    <th width="6%">Delete</th>
                                </tr>
                                <tr>
                                    <th class="text-center">
                                        <input type="checkbox" id="CheckAllShow" style="position: relative;left: 0px;opacity: 1;">
                                    </th>
                                    <th class="text-center">
                                        <input type="checkbox" id="CheckAllAdd" style="position: relative;left: 0px;opacity: 1;">
                                    </th>
                                    <th class="text-center">
                                        <input type="checkbox" id="CheckAllEdit" style="position: relative;left: 0px;opacity: 1;">
                                    </th>
                                    <th class="text-center">
                                        <input type="checkbox" id="CheckAllDetail" style="position: relative;left: 0px;opacity: 1;">
                                    </th>
                                    <th class="text-center">
                                        <input type="checkbox" id="CheckAllDelete" style="position: relative;left: 0px;opacity: 1;">
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 0 ?>
                                @foreach ($listUsersMenu as $key => $value)
                                <?php $no++; ?>
                                <tr>
                                    <td>{{$no}}</td>
                                    <td>{{$value->kode_menu}}</td>
                                    <td class="text-left">{{$value->name_menu}}</td>
                                    <td>{{$value->url}}</td>
                                    <td>
                                        <i class="material-icons">{{ $value->icon}}</i>
                                    </td>
                                    <td>{{$value->sequence}}</td>
                                    <td>
                                        <input type='checkbox' id='CheckSubShow' {{($value->xshow > 0) ?'checked' : null}} onclick='CheckTotalChecked("#CheckAllShow", "#CheckSubShow")' data-id='{{$value->id}}' style='position: relative;left: 0px;opacity: 1;' />
                                    </td>
                                    <td>
                                        <input type='checkbox' id='CheckSubAdd' {{($value->xadd > 0) ?'checked' : null}} onclick='CheckTotalChecked("#CheckAllAdd", "#CheckSubAdd")' data-id='{{$value->id}}' style='position: relative;left: 0px;opacity: 1;' />
                                    </td>
                                    <td>
                                        <input type='checkbox' id='CheckSubEdit' {{($value->xedit > 0) ?'checked' : null}} onclick='CheckTotalChecked("#CheckAllEdit", "#CheckSubEdit")' data-id='{{$value->id}}' style='position: relative;left: 0px;opacity: 1;' />
                                    </td>
                                    <td>
                                        <input type='checkbox' id='CheckSubDetail' {{($value->xdetail > 0) ?'checked' : null}} onclick='CheckTotalChecked("#CheckAllDetail", "#CheckSubDetail")' data-id='{{$value->id}}' style='position: relative;left: 0px;opacity: 1;' />
                                    </td>
                                    <td>
                                        <input type='checkbox' id='CheckSubDelete' {{($value->xdelete > 0) ?'checked' : null}} onclick='CheckTotalChecked("#CheckAllDelete", "#CheckSubDelete")' data-id='{{$value->id}}' style='position: relative;left: 0px;opacity: 1;' />
                                    </td>
                                </tr>
                                <?php
                                $GetUsersMenu_rel = \App\Http\Controllers\UsersManagement\UsersAccessController::GetUsersAccess_rel($value->id, @$myUsersLevel->id);
                                ?>
                                @foreach ($GetUsersMenu_rel as $k => $v)
                                <?php $no++; ?>
                                <tr>
                                    <td>{{$no}}</td>
                                    <td>{{$v->kode_menu}}</td>
                                    <td class="text-left">=== {{$v->name_menu}}</td>
                                    <td>{{$v->url}}</td>
                                    <td>
                                        <i class="material-icons">{{ $v->icon}}</i>
                                    </td>
                                    <td>{{$v->sequence}}</td>
                                    <td>
                                        <input type='checkbox' id='CheckSubShow' {{($v->xshow > 0) ?'checked' : null}} onclick='CheckTotalChecked("#CheckAllShow", "#CheckSubShow")' data-id='{{$v->id}}' style='position: relative;left: 0px;opacity: 1;' />
                                    </td>
                                    <td>
                                        <input type='checkbox' id='CheckSubAdd' {{($v->xadd > 0) ?'checked' : null}} onclick='CheckTotalChecked("#CheckAllAdd", "#CheckSubAdd")' data-id='{{$v->id}}' style='position: relative;left: 0px;opacity: 1;' />
                                    </td>
                                    <td>
                                        <input type='checkbox' id='CheckSubEdit' {{($v->xedit > 0) ?'checked' : null}} onclick='CheckTotalChecked("#CheckAllEdit", "#CheckSubEdit")' data-id='{{$v->id}}' style='position: relative;left: 0px;opacity: 1;' />
                                    </td>
                                    <td>
                                        <input type='checkbox' id='CheckSubDetail' {{($v->xdetail > 0) ?'checked' : null}} onclick='CheckTotalChecked("#CheckAllDetail", "#CheckSubDetail")' data-id='{{$v->id}}' style='position: relative;left: 0px;opacity: 1;' />
                                    </td>
                                    <td>
                                        <input type='checkbox' id='CheckSubDelete' {{($v->xdelete > 0) ?'checked' : null}} onclick='CheckTotalChecked("#CheckAllDelete", "#CheckSubDelete")' data-id='{{$v->id}}' style='position: relative;left: 0px;opacity: 1;' />
                                    </td>
                                </tr>
                                @endforeach

                                @endforeach
                            </tbody>
                        </table>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<div class="modal fade" id="modal-detail">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Detail Data</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <table class="table table-detail table-striped">
                    <tr>
                        <th width="5%">Kode menu</th>
                        <th width="1%">:</th>
                        <td class="kode_menu"></td>
                    </tr>
                    <tr>
                        <th>Name Menu</th>
                        <th>:</th>
                        <td class="name_menu"></td>
                    </tr>
                    <tr>
                        <th>Url</th>
                        <th>:</th>
                        <td class="url"></td>
                    </tr>
                    <tr>
                        <th>Sequence</th>
                        <th>:</th>
                        <td class="sequence"></td>
                    </tr>
                    <tr>
                        <th>Icon</th>
                        <th>:</th>
                        <td class="icon"></td>
                    </tr>
                    <tr>
                        <th>Parent</th>
                        <th>:</th>
                        <td class="parent_menu_name"></td>
                    </tr>
                    <tr>
                        <th>Created By</th>
                        <th>:</th>
                        <td class="created_by_name"></td>
                    </tr>
                    <tr>
                        <th>Created Date</th>
                        <th>:</th>
                        <td class="created_date"></td>
                    </tr>
                    <tr>
                        <th>Last Modified By</th>
                        <th>:</th>
                        <td class="last_modified_by_name"></td>
                    </tr>
                    <tr>
                        <th>Last Modified Date</th>
                        <th>:</th>
                        <td class="last_modified_date"></td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@endsection
@section('script')
<script>
    var table = "";
    var module_api = "<?php echo url('/users-access'); ?>";
    var save_type = "Add";
    var users_level_id = "{{@$myUsersLevel->id}}";
    $(document).ready(function() {
        @if(Session::has("success"))
        toastr.info("{{ Session::get("success") }}");
        @endif
        @if(Session::has("error"))
        toastr.error("{{ Session::get("error") }}");
        @endif
        $('#DataTable').wrap("<div class='table-responsive'></div>");
        CheckTotalChecked('#CheckAllShow', '#CheckSubShow');
        CheckTotalChecked('#CheckAllAdd', '#CheckSubAdd');        
        CheckTotalChecked('#CheckAllDetail', '#CheckSubDetail');
        CheckTotalChecked('#CheckAllEdit', '#CheckSubEdit');
        CheckTotalChecked('#CheckAllDelete', '#CheckSubDelete')
        $("#CheckAllShow").click(function() {
            $('#DataTable').find('input#CheckSubShow').not(this).prop('checked', this.checked);
        });
        $("#CheckAllAdd").click(function() {
            $('#DataTable').find('input#CheckSubAdd').not(this).prop('checked', this.checked);
        });
        $("#CheckAllDetail").click(function() {
            $('#DataTable').find('input#CheckSubDetail').not(this).prop('checked', this.checked);
        });
        $("#CheckAllEdit").click(function() {
            $('#DataTable').find('input#CheckSubEdit').not(this).prop('checked', this.checked);
        });
        $("#CheckAllDelete").click(function() {
            $('#DataTable').find('input#CheckSubDelete').not(this).prop('checked', this.checked);
        });
    });

    function CheckTotalChecked(all, sub) {
        var TotalOfCheckBoxRow = $('input' + sub).length;
        var TotalOfChecked = $('input' + sub + ':checked').length;
        if (TotalOfChecked > 0) {
            if (TotalOfCheckBoxRow == TotalOfChecked) {
                $(all).prop('checked', true);
            } else {
                $(all).prop('checked', false);
            }
        } else {
            $(all).prop('checked', false);
        }
    }

    function DeleteSelected() {
        var ValChecked = [];
        $('#CheckboxRow:checked').each(function() {
            ValChecked.push($(this).attr('data-id'));
        });
        Swal.fire({
            title: 'Are you sure?',
            text: "Delete Data",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!'
        }).then((result) => {
            console.log(result)
            if (result.value) {
                $.ajax({
                    url: module_api + "/delete_selected",
                    method: 'post',
                    data: {
                        id: ValChecked
                    },
                    success: function(data) {
                        if (data.error == 0) {
                            location.reload();
                        } else {
                            toastr.warning(data.message);
                        }
                        swal.close();
                    },
                    error: function(data) {
                        toastr.error("Error Delete Selected Data");
                        swal.close();
                    }
                });
            }
        })
    }

    function Save() {
        Swal.fire({
            title: 'Are you sure?',
            text: "Delete Data",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!'
        }).then((result) => {
            console.log(result)
            if (result.value) {
                var ValShow = [];
                var ValAdd = [];
                var ValDetail = [];
                var ValEdit = [];
                var ValDelete = [];
                $('#CheckSubShow:checked').each(function() {
                    ValShow.push($(this).attr('data-id'));
                });
                $('#CheckSubAdd:checked').each(function() {
                    ValAdd.push($(this).attr('data-id'));
                });
                $('#CheckSubDetail:checked').each(function() {
                    ValDetail.push($(this).attr('data-id'));
                });
                $('#CheckSubEdit:checked').each(function() {
                    ValEdit.push($(this).attr('data-id'));
                });
                $('#CheckSubDelete:checked').each(function() {
                    ValDelete.push($(this).attr('data-id'));
                });
                setTimeout(() => {
                    $.ajax({
                        url: module_api + "",
                        method: 'post',
                        data: {
                            access_show: ValShow,
                            access_add: ValAdd,
                            access_detail: ValDetail,
                            access_edit: ValEdit,
                            access_delete: ValDelete,
                            users_level_id: users_level_id
                        },
                        success: function(data) {
                            if (data.error == 0) {
                                location.reload();
                            } else {
                                toastr.warning(data.message)
                            }
                            swal.close();
                        },
                        error: function(data) {
                            toastr.error("Error Save");
                            swal.close();
                        }
                    });
                }, 150);
            }
        })
    }
</script>
@endsection