@extends('layouts.app', [
    'firstMenu' => $firstMenu,
    'secondMenu' => $secondMenu,
    'activePage' => 'element-masyarakat', 'titlePage' => __('Users Level')])

@section('content')
<style>
    #DataTable tbody tr td:nth-child(1),#DataTable tbody tr td:nth-child(2) {
        text-align: center;
    }
  </style>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title ">Data Users Level</h4>
                        {{-- <p class="card-category"> Here is a subtitle for this table</p> --}}
                    </div>
                    <div class="card-body">

                        @if ($access_add)
                            <button type="button" class="btn btn-sm btn-primary" onclick="Add()" data-trigger="hover" data-toggle="popover" data-placement="right" data-content="Add Data" data-original-title="">Add</button>
                        @endif

                        <button type="button" class="btn btn-info btn-sm" onclick="Refresh()" data-trigger="hover" data-toggle="popover" data-placement="right" data-content="Refresh Data"> Refresh</button>

                        @if ($access_delete)
                            <button type="button" id="DeleteSelected" class="btn btn-sm btn-danger" onclick="DeleteSelected()" data-trigger="hover" data-toggle="popover" data-placement="right" data-content="Delete Selected" style="display: none;">Delete Selected</button>
                        @endif

                        <table class="table table-striped table-hover DataTable" id="DataTable" style="width: 100%">
                            <thead>
                                <tr>
                                    <th width="5%" style="vertical-align: middle;">No.</th>
                                    <th width="5%" class="text-center">
                                        <input type="checkbox" id="CheckAll" style="position: relative;left: 0px;opacity: 1;">
                                    </th>
                                    <th width="5%" class="text-center">Opsi</th>
                                    <th>Level</th>
                                    <th width="5%">Sequence</th>
                                    <th width="10%">Allow Login</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-form">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Form</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <form id="form">
                @csrf
                <input type="hidden" name="id">
                <div class="modal-body">
                    <div class="form-group">
                        <label>Name Level</label>
                        <div class="form-line">
                            <input type="text" class="form-control" name="name_level" required maxlength="200">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Urutan</label>
                        <div class="form-line">
                            <input type="number" class="form-control" name="sequence" required maxlength="11">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Allow Login</label>
                        <div class="form-line">
                            <select class="form-control" name="allow_login" required>
                                <option value="">Pilih</option>
                                <option value="1">Ya</option>
                                <option value="0">Tidak</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-detail">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Detail Data</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <table class="table table-detail table-striped">
                    <tr>
                        <th width="5%">Level</th>
                        <th width="1%">:</th>
                        <td class="name_level"></td>
                    </tr>
                    <tr>
                        <th>Urutan</th>
                        <th>:</th>
                        <td class="sequence"></td>
                    </tr>
                    <tr>
                        <th>Allow Login</th>
                        <th>:</th>
                        <td class="allow_login"></td>
                    </tr>
                    <tr>
                        <th>Created By</th>
                        <th>:</th>
                        <td class="created_by_name"></td>
                    </tr>
                    <tr>
                        <th>Created Date</th>
                        <th>:</th>
                        <td class="created_date"></td>
                    </tr>
                    <tr>
                        <th>Last Modified By</th>
                        <th>:</th>
                        <td class="last_modified_by_name"></td>
                    </tr>
                    <tr>
                        <th>Last Modified Date</th>
                        <th>:</th>
                        <td class="last_modified_date"></td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@endsection
@section('script')
<script>
    var table = "";
    var module_api = "<?php echo url('/users-level'); ?>";
    var save_type = "Add";
    $(document).ready(function() {
        $('#form').on("submit", function(event) {
            event.preventDefault();
            var url;
            if (save_type == "Add") {
                var url = ""
            } else {
                var url = "/update"
            }
            ResetValidation();
            Swal.fire({
                title: 'Are you sure?',
                text: "Save Data",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes!'
            }).then((result) => {
                console.log(result)
                if (result.value) {
                    setTimeout(() => {
                        $.ajax({
                            url: module_api + url,
                            data: new FormData($('#form')[0]),
                            type: 'post',
                            contentType: false,
                            processData: false,
                            async: false,
                            dataType: 'json',
                            success: function(data) {
                                if (data.error == 0) {
                                    ResetForm();
                                    table.ajax.reload(null, false);
                                    $("#modal-form").modal('hide');
                                    toastr.info(data.message);
                                } else if (data.error == 1) {
                                    setTimeout(() => {
                                        $.each(data.error_validation, function(key, value) {
                                            ValidationPopover('[name=' + key + ']', value);
                                        });
                                    }, 0);
                                    toastr.warning(data.message);
                                } else {
                                    toastr.warning(data.message);
                                }
                                swal.close();
                            },
                            error: function(data) {
                                swal.close();
                                toastr.error("Error " + save_type + " Data");
                            }
                        });
                    }, 150);
                }
            })
        });
        table = $('#DataTable').DataTable({
            'lengthMenu': [
                [10, 25, 50, 100, 200, 350, -1],
                [10, 25, 50, 100, 200, 350, "All"]
            ],
            "language": {
                "emptyTable": "<center>Data not found</center>",
                "processing": '<center><i class="fa fa-refresh fa-spin fa-3x fa-fw" style="font-size:16pt;"></i></center>',

            },
            'searching': true,
            'processing': true,
            'serverSide': true,
            "orderCellsTop": true,
            "paging": true,
            'order': [],
            'ajax': {
                url: module_api + '/list',
                type: "POST",
                data: function(d) {
                    d.name_level = "";
                },
                "error": function(jqXHR) {}
            },
            "fnDrawCallback": function(oSettings) {
                $('#DataTable').find('input:checkbox').prop('checked', false);
                CheckTotalCheckedDelete();
            },
            "columns": [{
                    "data": null,
                    render: function(data, type, row, meta) {
                        return +meta.row + meta.settings._iDisplayStart + 1;
                    },
                },
                {
                    "data": "id",
                    render: function(data, type, row, meta) {
                        return "<input type='checkbox' id='CheckboxRow' onclick='CheckTotalCheckedDelete()' data-id='" + row.id + "' style='position: relative;left: 0px;opacity: 1;'/>";
                    },
                },
                {
                    "data": "opsi"
                },
                {
                    "data": "name_level"
                },
                {
                    "data": "sequence"
                },
                {
                    "data": "allow_login"
                },
            ],
            "columnDefs": [
                {
                    "searchable": false,
                    "orderable": false,
                    "targets": [0, 1, 2],
                }
                @if (!$access_delete)
                    ,{
                        "targets": [1],
                        "visible": false
                    }
                @endif
            ],
        });
        $('#DataTable').wrap("<div class='table-responsive'></div>");
        $('#DeleteSelected').fadeOut(0);
        $("#CheckAll").click(function() {
            $('#DataTable').find('input:checkbox').not(this).prop('checked', this.checked);
            CheckTotalCheckedDelete();
        });
    });

    function Refresh() {
        table.ajax.reload(null, false);
    }

    function CheckTotalCheckedDelete() {
        var TotalOfCheckBoxRow = $('input#CheckboxRow').length;
        var TotalOfChecked = $('input#CheckboxRow:checked').length;
        if (TotalOfChecked > 0) {
            $('#DeleteSelected').fadeIn(0);
            if (TotalOfCheckBoxRow == TotalOfChecked) {
                $('#CheckAll').prop('checked', true);
            } else {
                $('#CheckAll').prop('checked', false);
            }
        } else {
            $('#DeleteSelected').fadeOut(0);
            $('#CheckAll').prop('checked', false);
        }
    }

    function Reset() {
        if (save_type == "Update") {
            ResetForm();
        }
        setTimeout(() => {}, 0);
    }

    function ResetValidation() {
        $('#form').find('input').popover('dispose');
        $('#form').find('select').popover('dispose');
    }

    function ResetForm() {
        $('#form')[0].reset();
        ResetValidation();
        setTimeout(() => {}, 0);
    }

    function Add() {
        Reset();
        save_type = "Add";
        $("#modal-form").modal('show');
    }

    function Edit(value) {
        ResetForm();
        $.ajax({
            url: module_api + "/where",
            type: 'GET',
            data: {
                id: value,
            },
            success: function(data) {
                if (data.error == 0) {
                    var d = data.data[0];
                    $('[name=id]').val(d.id);
                    $('[name=name_level]').val(d.name_level);
                    $('[name=sequence]').val(d.sequence);
                    $('[name=allow_login]').val(d.allow_login);
                    $("#modal-form").modal('show');
                    save_type = "Update";
                } else {
                    toastr.warning(data.message);
                }
            },
            error: function(data) {
                toastr.error("Error Edit Data");
            }
        });
    }
    
    function Detail(value){
        $.ajax({
            url:module_api+"/where",
            type:'GET',
            data:{
                id:value,
            },
            success: function(data){
                if (data.error == 0) {
                    $("#modal-detail").modal('show');
                    var d = data.data[0];
                    $('.name_level').text(d.name_level);
                    $('.sequence').text(d.sequence);
                    $('.allow_login').text((d.allow_login == 1) ? "Ya" : "Tidak");
                    $('.created_by_name').text((d.created_by_user) ? d.created_by_user.name : null);
                    $('.created_date').text(d.created_date);
                    $('.last_modified_by_name').text((d.last_modified_by_user) ? d.last_modified_by_user.name : null);
                    $('.last_modified_date').text(d.last_modified_date);
                }else{
                    toastr.warning(data.message);
                }
            },
            error:function(data){
                toastr.error("Error Detail Data");
            }
        });
    }

		function Delete(value) {
            Swal.fire({
                title: 'Are you sure?',
                text: "Delete Data",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes!'
            }).then((result) => {
                console.log(result)
                if (result.value) {		
					$.ajax({
						url:module_api+"/"+value,
						method:'delete',
						success:function(data){
							if (data.error == 0) {
								table.ajax.reload(null, false);
								toastr.info(data.message);
							}else{
								toastr.warning(data.message);
							}
	                        swal.close();
						},
						error:function(data){
								toastr.error('Error Delete Data');
	                        swal.close();
						}
					});
				}
			});
		}

		function DeleteSelected(){
            var ValChecked = [];
            $('#CheckboxRow:checked').each(function() {
                ValChecked.push($(this).attr('data-id'));
            });
            Swal.fire({
                title: 'Are you sure?',
                text: "Delete Data",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes!'
            }).then((result) => {
                console.log(result)
                if (result.value) {			
					$.ajax({
						url: module_api+"/delete_selected",
						method:'post',
						data:{id:ValChecked},
						success:function(data){
							if (data.error == 0) {
								table.ajax.reload(null, false);
					    		toastr.info(data.message);
							}else{
								toastr.warning(data.message);
							}
	                        swal.close();
						},
						error:function(data){
                            toastr.error("Error Delete Selected Data");
	                        swal.close();
						}
					});
				}
			})
		}
</script>
@endsection