<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Auth::routes();



Route::get('/login', [App\Http\Controllers\Auth\LoginController::class, 'index'])->name('login');
Route::post('/login', [App\Http\Controllers\Auth\LoginController::class, 'proses_login']);
Route::post('/logout', [App\Http\Controllers\Auth\LoginController::class, 'logout'])->name('logout');
Route::get('/register', [App\Http\Controllers\Auth\RegisterController::class, 'index'])->name('register');
Route::post('/register', 'App\Http\Controllers\Auth\RegisterController@add');

Route::group(['middleware' => 'auth'], function () {
	Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

	Route::get('element-masyarakat', 'App\Http\Controllers\RefElementMasyarakatController@index')->name('element-masyarakat');
	Route::get('/element-masyarakat/where', 'App\Http\Controllers\RefElementMasyarakatController@where');
	Route::post('/element-masyarakat/list', 'App\Http\Controllers\RefElementMasyarakatController@list');
	Route::post('/element-masyarakat', 'App\Http\Controllers\RefElementMasyarakatController@add');
	Route::post('/element-masyarakat/update', 'App\Http\Controllers\RefElementMasyarakatController@update');
	Route::delete('/element-masyarakat/{id}', 'App\Http\Controllers\RefElementMasyarakatController@delete');
	Route::post('/element-masyarakat/delete_selected', 'App\Http\Controllers\RefElementMasyarakatController@delete_selected');

	Route::get('perangkat-daerah', 'App\Http\Controllers\RefPerangkatDaerahController@index')->name('perangkat-daerah');
	Route::get('/perangkat-daerah/where', 'App\Http\Controllers\RefPerangkatDaerahController@where');
	Route::post('/perangkat-daerah/list', 'App\Http\Controllers\RefPerangkatDaerahController@list');
	Route::post('/perangkat-daerah', 'App\Http\Controllers\RefPerangkatDaerahController@add');
	Route::post('/perangkat-daerah/update', 'App\Http\Controllers\RefPerangkatDaerahController@update');
	Route::delete('/perangkat-daerah/{id}', 'App\Http\Controllers\RefPerangkatDaerahController@delete');
	Route::post('/perangkat-daerah/delete_selected', 'App\Http\Controllers\RefPerangkatDaerahController@delete_selected');

	Route::get('klasifikasi-kategori', 'App\Http\Controllers\RefKlasifikasiKategoriController@index')->name('klasifikasi-kategori');
	Route::get('/klasifikasi-kategori/where', 'App\Http\Controllers\RefKlasifikasiKategoriController@where');
	Route::post('/klasifikasi-kategori/list', 'App\Http\Controllers\RefKlasifikasiKategoriController@list');
	Route::post('/klasifikasi-kategori', 'App\Http\Controllers\RefKlasifikasiKategoriController@add');
	Route::post('/klasifikasi-kategori/update', 'App\Http\Controllers\RefKlasifikasiKategoriController@update');
	Route::delete('/klasifikasi-kategori/{id}', 'App\Http\Controllers\RefKlasifikasiKategoriController@delete');
	Route::post('/klasifikasi-kategori/delete_selected', 'App\Http\Controllers\RefKlasifikasiKategoriController@delete_selected');

	Route::get('users', 'App\Http\Controllers\UsersManagement\UsersController@index')->name('users');
	Route::get('users/where', 'App\Http\Controllers\UsersManagement\UsersController@where');
	Route::post('users/list', 'App\Http\Controllers\UsersManagement\UsersController@list');
	Route::post('users', 'App\Http\Controllers\UsersManagement\UsersController@add');
	Route::post('users/update', 'App\Http\Controllers\UsersManagement\UsersController@update');
	Route::delete('users/{id}', 'App\Http\Controllers\UsersManagement\UsersController@delete');
	Route::post('users/delete_selected', 'App\Http\Controllers\UsersManagement\UsersController@delete_selected');
	Route::post('users/login_as/{id}', 'App\Http\Controllers\UsersManagement\UsersController@login_as');

	Route::get('users-level', 'App\Http\Controllers\UsersManagement\UsersLevelController@index')->name('users-level');
	Route::get('users-level/where', 'App\Http\Controllers\UsersManagement\UsersLevelController@where');
	Route::post('users-level/list', 'App\Http\Controllers\UsersManagement\UsersLevelController@list');
	Route::post('users-level', 'App\Http\Controllers\UsersManagement\UsersLevelController@add');
	Route::post('users-level/update', 'App\Http\Controllers\UsersManagement\UsersLevelController@update');
	Route::delete('users-level/{id}', 'App\Http\Controllers\UsersManagement\UsersLevelController@delete');
	Route::post('users-level/delete_selected', 'App\Http\Controllers\UsersManagement\UsersLevelController@delete_selected');

	Route::get('users-menu', 'App\Http\Controllers\UsersManagement\UsersMenuController@index')->name('users-menu');
	Route::get('users-menu/where', 'App\Http\Controllers\UsersManagement\UsersMenuController@where');
	Route::post('users-menu', 'App\Http\Controllers\UsersManagement\UsersMenuController@add');
	Route::post('users-menu/update', 'App\Http\Controllers\UsersManagement\UsersMenuController@update');
	Route::delete('users-menu/{id}', 'App\Http\Controllers\UsersManagement\UsersMenuController@delete');
	Route::post('users-menu/delete_selected', 'App\Http\Controllers\UsersManagement\UsersMenuController@delete_selected');

	Route::get('users-level-group/where', 'App\Http\Controllers\UsersManagement\UsersLevelGroupController@where');

	Route::get('users-access/where', 'App\Http\Controllers\UsersManagement\UsersAccessController@where');
	Route::get('users-access', 'App\Http\Controllers\UsersManagement\UsersAccessController@index')->name('users-access');
	Route::post('users-access', 'App\Http\Controllers\UsersManagement\UsersAccessController@save')->name('users-access-save');

	Route::get('pengguna/daftar', 'App\Http\Controllers\PenggunaController@index')->name('pengguna-daftar');
	Route::post('pengguna/list', 'App\Http\Controllers\PenggunaController@list');
	Route::post('pengguna', 'App\Http\Controllers\PenggunaController@add');
	Route::post('pengguna/update', 'App\Http\Controllers\PenggunaController@update');
	Route::delete('pengguna/{id}', 'App\Http\Controllers\PenggunaController@delete');
	Route::post('pengguna/delete_selected', 'App\Http\Controllers\PenggunaController@delete_selected');
	Route::post('pengguna/login_as/{id}', 'App\Http\Controllers\PenggunaController@login_as');


	Route::get('pengguna/konfirmasi', 'App\Http\Controllers\PenggunaKonfirmasiController@index')->name('pengguna-konfirmasi');
	Route::post('pengguna/konfirmasi/list', 'App\Http\Controllers\PenggunaKonfirmasiController@list');
	Route::put('pengguna/konfirmasi/{id}/{is_konfirmasi}', 'App\Http\Controllers\PenggunaKonfirmasiController@konfirmasi');
	Route::post('pengguna/konfirmasi/selected', 'App\Http\Controllers\PenggunaKonfirmasiController@konfirmasi_selected');

	Route::get('pengguna/tertolak', 'App\Http\Controllers\PenggunaTertolakController@index')->name('pengguna-konfirmasi');
	Route::post('pengguna/tertolak/list', 'App\Http\Controllers\PenggunaTertolakController@list');
	Route::put('pengguna/tertolak/{id}/{is_konfirmasi}', 'App\Http\Controllers\PenggunaTertolakController@konfirmasi');
	Route::post('pengguna/tertolak/selected/', 'App\Http\Controllers\PenggunaTertolakController@konfirmasi_selected');

	Route::put('/switch_level/{users_level_id}', 'App\Http\Controllers\UsersManagement\UsersController@switch_level');

	Route::get('/inovasi', [App\Http\Controllers\InovasiController::class, 'index'])->name('inovasi');
	Route::get('/inovasi/where', [App\Http\Controllers\InovasiController::class, 'where']);
	Route::post('/inovasi/list', [App\Http\Controllers\InovasiController::class, 'list']);
	Route::post('/inovasi', [App\Http\Controllers\InovasiController::class, 'add']);
	Route::post('/inovasi/update', [App\Http\Controllers\InovasiController::class, 'update']);
	Route::delete('/inovasi/{id}', [App\Http\Controllers\InovasiController::class, 'delete']);
	Route::get('/inovasi/proposal/{id}', [App\Http\Controllers\InovasiController::class, 'proposal']);
	Route::post('/inovasi/proposal', [App\Http\Controllers\InovasiController::class, 'update_proposal']);
	Route::post('/inovasi/delete_selected', [App\Http\Controllers\InovasiController::class, 'delete_selected']);


	Route::get('table-list', function () {
		return view('pages.table_list');
	})->name('table');

    Route::get('proposal', function () {
		return view('pages.judul-proposal');
	})->name('table');

	Route::get('typography', function () {
		return view('pages.typography');
	})->name('typography');

	Route::get('icons', function () {
		return view('pages.icons');
	})->name('icons');

	Route::get('map', function () {
		return view('pages.map');
	})->name('map');

	Route::get('notifications', function () {
		return view('pages.notifications');
	})->name('notifications');

	Route::get('rtl-support', function () {
		return view('pages.language');
	})->name('language');

	Route::get('upgrade', function () {
		return view('pages.upgrade');
	})->name('upgrade');
});

Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'App\Http\Controllers\UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'App\Http\Controllers\ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'App\Http\Controllers\ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'App\Http\Controllers\ProfileController@password']);
});

